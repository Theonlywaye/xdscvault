ConvertFrom-StringData @'
    ObtainClientToken = Obtaining client token
    RetrieveCurrentCalue = Retrieving current value for {0}
    VaultValueMatchesSupplied = Value supplied matches what is currently already in Vault for {0}
    VaultValueDoesNotMatchSupplied = Value supplied does not match what is currently already in Vault for {0}
    Returned404 = Returned 404. No value found at {0}
    UnknownError = Unknown error
    ValueEmptywithRandomSecretTrue = Vault value is empty and random password flag is true
    ValueEmptywithRandomSecretTrueWithForce = Random secret with force update
    ValueEmptywithRandomSecretTrueNoForce = Random secret without force update and current value exists
    VaultValueTruewithRandomSecretTrue = VaultValue true and RandomSecret true. Use one or the other
    VaultValueFalsewithRandomSecretFalse = VaultValue false and RandomSecret false. Pick one
    VaultValueTruewithRandomSecretFalse = VaultValue true and RandomSecret false
    AllValuesEmptywithRandomSecretTrueNoForce = All values are empty with random secret true with no force
    DecryptingCurrentValueSecureString = Decrypting current value secure string
    DecryptingClientTokenSecureString = Decrypting client token secure string
    NoCurrentValueRetrieved = There was no current value present at {0}
    ClientTokenMissing = The client token is missing
'@