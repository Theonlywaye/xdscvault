ConvertFrom-StringData @'
    VaultDnsException = Vault address could not be resolved
    VaultDnsTimeout = Attempted to get HEAD from Vault URL but timed out
    VaultTokenLookupInvalid = Wrapped token has already been unwrapped, not valid or does not exist 
    IsVaulttokenwrapped = Checking to see if secret is still wrapped
    Vaulttokenisstillwrapped = Secret is still wrapped
    CheckingDNS = Checking DNS for '{0}'
    AttemptUnwrap = Attempting to unwrap token
    Createstoredcred = Creating credential in credential manager
    Deletestoredcred = Attempting to remove existing stored credential
    Resultwrapped = wrapped
    Resulttokenerror = tokenError
    Notokenerrorandnocredspresent = No token error and no stored credentials are present    
    Notokenerrorandcredspresent = No token error and stored credentials are present
    Tokenerrorwithstoredcredspresent = Token error but there are stored credentials present for target '{0}'
    Tokenerrorwithnostoredcredspresent = Token error and there no are stored credentials present for target '{0}'    
'@